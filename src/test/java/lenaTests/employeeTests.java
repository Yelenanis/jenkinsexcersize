package lenaTests;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.javainuse.service.EmployeeServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class employeeTests {


    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    EmployeeServiceImpl emplService;
    
    @Test
    public void getsAllEmployee() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/employees")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }
    
    @Test
    public void addEmployee() throws Exception {
         String newEmpl = "{\"empId\":\"00000\",\"empName\":\"Test Employee\"}";
        mockMvc.perform(MockMvcRequestBuilders.post("/insertemployee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newEmpl)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }
    
}
